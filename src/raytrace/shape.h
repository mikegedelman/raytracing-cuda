#ifndef SHAPE_H
#define SHAPE_H

#include <vector>
#include <memory>

#include <taggedptr.h>
#include <vec.h>
#include <raytrace/ray.h>
#include <raytrace/material.h>
#include <raytrace/intersection.h>
#include <raytrace/aabb.h>

enum ShapeType {
    sphere_type,
    triangle_type
};

class Sphere;
class Triangle;
class BVHNode;

class Intersection;

class Shape : public TaggedPointer<Sphere, Triangle, BVHNode> {
public:
    using TaggedPointer::TaggedPointer;
    CPU_GPU
    bool intersect(const Ray& ray, float t_min, float t_max, Intersection &isect) const;

    bool bounding_box(AABB& aabb) const;

    CPU_GPU2 bool is_primitive() const;

    void print() const;
};


class Sphere  {
public:
    Point3f center;
    float radius;
    Material material;

    CPU_GPU
    Sphere() {}

    CPU_GPU
    Sphere(Point3f center, float radius, Material material)
    : center(center), radius(radius), material(material) {};

    CPU_GPU
    bool intersect(const Ray& ray, float t_min, float t_max, Intersection &isect) const;

    bool bounding_box(AABB& aabb) const;

    void print() const;

    CPU_GPU2 bool is_primitive() const { return true; }
    // CPU_GPU
    // virtual void print() const override;
};

class Triangle  {
public:
    Point3f v0;
    Point3f v1;
    Point3f v2;
    Material material;

    CPU_GPU
    Triangle() {}

    CPU_GPU
    Triangle(Point3f v0, Point3f v1, Point3f v2, Material material) : v0(v0), v1(v1), v2(v2), material(material) {};

    CPU_GPU
    bool intersect(const Ray& ray, float t_min, float t_max, Intersection &isect) const;

    bool bounding_box(AABB& aabb) const;

    CPU_GPU2 bool is_primitive() const { return true; }

    void print() const;

    // CPU_GPU
    // virtual void print() const override;

    CPU_GPU
    Vec3f get_barycentric(const Point3f &p) const;

    void rotate_x(float degrees);
    void rotate_y(float degrees);
    void rotate_z(float degrees);
    void translate(Vec3f v);
    void scale(float amount);
    void stretch(Vec3f magnitudes);
};


class BVHNode {
public:
    BVHNode() {};

    BVHNode(std::vector<Shape>& src_objects)
        : BVHNode(src_objects, 0, src_objects.size()) {};

    BVHNode(
        std::vector<Shape>& src_objects,
        size_t start,
        size_t end
    );

    CPU_GPU
    bool intersect(const Ray& ray, float t_min, float t_max, Intersection &isect) const;

    bool bounding_box(AABB& aabb) const;

    CPU_GPU2 bool is_primitive() const { return false; }

    void print() const;

    Shape left;
    Shape right;
    AABB box;
};

#endif
