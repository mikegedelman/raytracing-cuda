#ifndef MESH_H
#define MESH_H

#include <iostream>
#include <string>
#include <vector>

#include <util.h>
#include <raytrace/shape.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>


class Mesh {
public:
    std::vector<Shape> triangles;

    Mesh() {};

    Mesh(const char* filename, Material mat) : triangles() {
        Assimp::Importer importer;
        const aiScene* scene = importer.ReadFile(std::string(filename), 0);

        if (scene == nullptr) {
            std::cerr << "err" << std::endl;
            std::cerr << importer.GetErrorString() << std::endl;
            delete scene;
            std::string err_msg = std::string("Unable to load asset at path: ") + std::string(filename);
            throw std::runtime_error(err_msg);
        }

        const aiMesh* mesh = scene->mMeshes[0];
        triangles.reserve(mesh->mNumFaces);

        for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
            aiFace* f = &mesh->mFaces[i];

            aiVector3D* v = &mesh->mVertices[f->mIndices[0]];
            auto v0 = Vec3f(v->x, v->y, v->z);
            v = &mesh->mVertices[f->mIndices[1]];
            auto v1 = Vec3f(v->x, v->y, v->z);
            v = &mesh->mVertices[f->mIndices[2]];
            auto v2 = Vec3f(v->x, v->y, v->z);

            auto t = new_object<Triangle>(v0, v1, v2, mat);
            triangles.push_back(t);
        }
    }

    static Mesh create_plane(Material mat) { // TODO: aspect ratio
        Mesh ret;
        Point3f top_left(-1.f, 1.f, 0.f);
        Point3f bottom_left(-1.f, -1.f, 0.f);
        Point3f bottom_right(1.f, -1.f, 0.f);
        Point3f top_right(1.f, 1.f, 0.f);

        // top left
        ret.triangles.push_back(new_object<Triangle>(top_left, bottom_left, bottom_right, mat));

        // bottom right
        ret.triangles.push_back(new_object<Triangle>(bottom_right, top_right, top_left, mat));

        return ret;
    }

    static Mesh create_box(float x_magnitude, float y_magnitude, float z_magnitude, Material mat) { // TODO: aspect ratio
        Mesh ret;
        Point3f top_left(-1.f, 1.f, 0.f);
        Point3f bottom_left(-1.f, -1.f, 0.f);
        Point3f bottom_right(1.f, -1.f, 0.f);
        Point3f top_right(1.f, 1.f, 0.f);

        // top left
        ret.triangles.push_back(new_object<Triangle>(top_left, bottom_left, bottom_right, mat));

        // bottom right
        ret.triangles.push_back(new_object<Triangle>(bottom_right, top_right, top_left, mat));

        return ret;
    }

    void rotate_x(float degrees) {
        for (auto s : triangles) {
            Triangle *t = s.Cast<Triangle>();
            t->rotate_x(degrees);
        }
    }
    void rotate_y(float degrees) {
        for (auto s : triangles) {
            Triangle *t = s.Cast<Triangle>();
            t->rotate_y(degrees);
        }
    }
    void rotate_z(float degrees) {
        for (auto s : triangles) {
            Triangle *t = s.Cast<Triangle>();
            t->rotate_z(degrees);
        }
    }
    void translate(Vec3f v) {
        for (auto s : triangles) {
            Triangle *t = s.Cast<Triangle>();
            t->translate(v);
        }
    }
    void scale(float amount) {
        for (auto s : triangles) {
            Triangle *t = s.Cast<Triangle>();
            t->scale(amount);
        }
    }
    void stretch(Vec3f magnitudes) {
        for (auto s : triangles) {
            Triangle *t = s.Cast<Triangle>();
            t->stretch(magnitudes);
        }
    }
};

#endif
