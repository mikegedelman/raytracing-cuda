#ifndef IMG_H
#define IMG_H

#include <vec.h>
#include <raytrace/camera.h>
#include <raytrace/shape.h>
#include <raytrace/world.h>

class World;

class ImageConfig {
public:
  int width;
  int height;
  float aspect_ratio;
  int samples;
  int max_depth;
  Camera camera;
  World* world;

  ImageConfig(int width, float aspect_ratio, int samples, int max_depth, Camera camera, World* world) :
    width(width),
    height(int(width / aspect_ratio)),
    aspect_ratio(aspect_ratio),
    samples(samples),
    max_depth(max_depth),
    camera(camera),
    world(world)
    {};
};

void write_image_png(ImageConfig &cfg, const char* filename, Vec3f *buf);
void write_image_ppm(ImageConfig &cfg, Vec3f *buf);

#endif
