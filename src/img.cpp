#include <iostream>
#include <cstdint>

// #define STB_IMAGE_IMPLEMENTATION
// #include <stb_image.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

#include <vec.h>
#include <img.h>
#include <util.h>

inline Vec3<uint8_t> vec3f_to_8bit_rgb(Vec3f v) {
  int ir = static_cast<int>(256 * clamp(v.x, 0.0f, 0.999f));
  int ig = static_cast<int>(256 * clamp(v.y, 0.0f, 0.999f));
  int ib = static_cast<int>(256 * clamp(v.z, 0.0f, 0.999f));

  return Vec3<uint8_t>(ir, ig, ib);
}


void write_image_png(ImageConfig &cfg, const char* filename, Vec3f *vec_buf) {
  uint8_t *buf = (uint8_t*) calloc(cfg.width * 3 * cfg.height, sizeof(uint8_t));
  size_t buf_offset = 0;
  // Quick and ugly way to copy an array of Vec3f to a packed buffer
  // for png output.
  for (int j = cfg.height - 1; j >= 0; j--) {
    for (int i = 0; i < cfg.width; i++) {
      Vec3<uint8_t> vi = vec3f_to_8bit_rgb(vec_buf[(j * cfg.width) + i]);
      buf[buf_offset++] = vi.x;
      buf[buf_offset++] = vi.y;
      buf[buf_offset++] = vi.z;
    }
  }

  stbi_write_png(filename, cfg.width, cfg.height, 3, buf, cfg.width * 3);
  std::cerr << "Wrote " << filename << std::endl;
}


// Writes to stdout.
void write_image_ppm(ImageConfig &cfg, Vec3f *buf) {
  std::cout << "P3\n" << cfg.width << ' ' << cfg.height << "\n255\n";
  for (int j = cfg.height - 1; j >= 0; j--) {
    for (int i = 0; i < cfg.width; i++) {
       Vec3<uint8_t> vi = vec3f_to_8bit_rgb(buf[(j * cfg.width) + i]);
       std::cout << int(vi.x) << " " << int(vi.y) << " " << int(vi.z) << "\n";
    }
  }
  std::cout << std::endl;
}
