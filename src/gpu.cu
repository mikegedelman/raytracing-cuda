#include <defines.h>
#ifdef USE_GPU

#include <gpu.h>
#include <img.h>
#include <vec.h>
#include <raytrace/camera.h>
#include <raytrace/raytrace.h>

#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>
#include <cuda.h>

#include <curand.h>
#include <curand_kernel.h>

__device__ curandState* curand_state;
__device__ int img_width; // used to calculate thread id later

__global__
void setup_kernel(ImageConfig* cfg, curandState *state) {
  int i = blockDim.x * blockIdx.x + threadIdx.x;
  if (i > cfg->width) return;

  int j = blockDim.y * blockIdx.y + threadIdx.y;
  if (j > cfg->height) return;

  int id = (j * cfg->width) + i;

  curand_init(7+id, id, 0, &state[id]);

  // Set globals for state, and img width, which is used to calculate the
  // thread id when using curand_state.
  curand_state = state;
  img_width = cfg->width;
}

__global__
void raytrace_kernel(ImageConfig *cfg, Vec3f *buf)
{
  int i = blockDim.x * blockIdx.x + threadIdx.x;
  if (i > cfg->width) return;

  int j = blockDim.y * blockIdx.y + threadIdx.y;
  if (j > cfg->height) return;

  int id = (j * cfg->width) + i;

  buf[id] = raytrace(*cfg, i, j);
}

void raytrace_gpu(ImageConfig* cfg) {
  int nx = cfg->width;
  int ny = cfg->height;

  Vec3f *buf;
  CUDA_CHECK(cudaMallocManaged((void **) &buf, sizeof(Vec3f) * nx * ny) );

  curandState *devStates;
  CUDA_CHECK(
    cudaMalloc((void**) &devStates, sizeof(curandState) * nx * ny)
  );

  dim3 blksz (8, 8, 1);
  dim3 grdsz ((nx + blksz.x - 1) / blksz.x, (ny + blksz.y - 1) / blksz.y, 1);

  setup_kernel<<<grdsz, blksz>>>(cfg, devStates);
  CUDA_CHECK( cudaPeekAtLastError() );
  raytrace_kernel<<<grdsz, blksz>>>(cfg, buf);
  CUDA_CHECK( cudaPeekAtLastError() );

  CUDA_CHECK( cudaDeviceSynchronize() );

  write_image_png(*cfg, "out.png", buf);

  CUDA_CHECK( cudaFree(buf) );
  CUDA_CHECK( cudaFree(devStates) );
}

#endif
