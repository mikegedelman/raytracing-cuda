#ifndef GPU_H
#define GPU_H

#include <stdio.h>

class ImageConfig;

void raytrace_gpu(ImageConfig *cfg);

__device__ float gpu_rand();

#define CUDA_CHECK(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

#endif
