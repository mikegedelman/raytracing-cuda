#include <iostream>
#include <vector>
#include <algorithm>
#include <time.h>
#include <math.h>

#include <defines.h>
#include <img.h>
#include <vec.h>
#include <gpu.h>
#include <util.h>
#include <rand.h>
#include <raytrace/mesh.h>
#include <raytrace/raytrace.h>
#include <raytrace/world.h>

#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <curand.h>
#include <curand_kernel.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <taggedptr.h>

#ifndef USE_GPU
#include <thread>

constexpr int parallelism = 16;

void raytrace_cpu_worker(ImageConfig& cfg, int thread_num, int size, Vec3f* buf) {
  auto j_start = thread_num * size;
  auto j_until = std::min(cfg.height, (j_start + size));
  for (int j = j_start; j < j_until; j++) {
    for (int i = 0; i < cfg.width; i++) {
      buf[(j * cfg.width) + i] = raytrace(cfg, i, j);
    }
  }
}

void raytrace_cpu(ImageConfig& cfg) {
  Vec3f *buf = (Vec3f*) calloc(cfg.width * cfg.height, sizeof(Vec3f));

  //const int processor_count = static_cast<int>(std::thread::hardware_concurrency());
  if (parallelism > 1) {
      std::cout << "Using " << parallelism << " threads." << std::endl;
      int scanlines_per_worker = (cfg.height + parallelism - 1) / parallelism;

      std::vector<std::thread> threads;
      for (auto s = 0; s < parallelism; s++) {
        threads.push_back(std::thread(raytrace_cpu_worker, cfg, s, scanlines_per_worker, buf));
      }
      for (auto& thread : threads) {
          thread.join();
      }
  }
  else {
      raytrace_cpu_worker(cfg, 0, cfg.height, buf);
  }
  write_image_png(cfg, "out.png", buf);
  // write_image_ppm(cfg, buf);
}
#endif

class WorldResources {
  std::vector<Shape> shapes;
  std::vector<Material> materials;
public:

  WorldResources() : shapes(), materials() {}
  WorldResources(std::vector<Shape> shapes, std::vector<Material> materials) : shapes(shapes), materials(materials) {}

  std::vector<Shape>& get_shapes() { return shapes; }

  void free() {
    for (auto shape : shapes) {
      delete_object(shape.ptr());
    }
    for (auto material : materials) {
      delete_object(material.ptr());
    }
  }

  void add_shape(Shape s) {
    shapes.push_back(s);
  }

  void add_mesh(Mesh &m) {
    shapes.insert(shapes.end(), m.triangles.begin(), m.triangles.end());
  }

  template <class T, class... Args>
  T* new_material(Args &&...args) {
    T* mat = new_object<T>(std::forward<Args>(args)...);
    materials.push_back(mat);
    return mat;
  }
};

// WorldResources* setup_world_rtweekend() {
//   WorldResources* res = new WorldResources;

//   Material ground = new_object<Lambertian>(Color(0.5, 0.5f, 0.5f));
//   res->add_shape(new_object<Sphere>(Point3f(0.f, -1000.f, 0.f), 1000.f, ground));

//   Vec3f top = Point3f(0.f, 1.1f, 0.f);
//   Vec3f right = Point3f(1.1f, -1.1f, 0.f);
//   Vec3f left  = Point3f(-1.1f, -1.1f, 0.f);
//   Vec3f back = Point3f(0.f, -1.1f, -1.1f);

//   Lambertian *black = new_object<Lambertian>(Color(.01f, .01f, .01f));
//   res->materials.push_back(black);
//   Metal *mirror = new_object<Metal>(Color(0.95f, 0.95f, 0.95f), 0.001f);
//   res->materials.push_back(mirror);

//   Triangle* t1 = new_object<Triangle>(top, left, right, mirror);
//   t1->rotate_y(105);
//   t1->translate(Vec3f(-5.f, 1.15f, 2.1f));
//   res->add_shape(t1);

//   Triangle* t2 = new_object<Triangle>(top * 1.1f, left * 1.1f, right * 1.1f, black);
//   t2->rotate_y(105);
//   t2->translate(Vec3f(-5.05f, 1.15f, 2.1f));
//   res->add_shape(t2);

//   for (int a = -11; a < 11; a++) {
//    for (int b = -11; b < 11; b++) {
//      float choose_mat = host_random_float();
//      Point3f center(
//        a + 0.9f * host_random_float(),
//        0.2f,
//        b + 0.9f * host_random_float()
//      );

//      if (length(center - Point3f(4.f, 0.2f, 0.f)) > 0.9f) {
//        Material mat;

//        if (choose_mat < 0.8) {
//          Color albedo = host_random_vec3f() * host_random_vec3f();
//          mat = new_object<Lambertian>(albedo);
//        } else if (choose_mat < 0.95) {
//          auto albedo = host_random_vec3f(0.5, 1.f);
//          auto fuzz = host_random_float(0.f, 0.5f);
//          mat = new_object<Metal>(albedo, fuzz);
//        } else {
//          mat = new_object<Dielectric>(1.5f);
//        }

//        res->add_shape(new_object<Sphere>(center, 0.2f, mat));
//        res->materials.push_back(mat);
//      }
//    }
//   }

//   Dielectric* mat1 = new_object<Dielectric>(1.5f);
//   res->materials.push_back(mat1);
//   res->add_shape(new_object<Sphere>(Point3f(0.f, 1.f, 0.f), 1.f, mat1));

//   Lambertian* mat2 = new_object<Lambertian>(Color(0.4f, 0.2f, 0.1f));
//   res->materials.push_back(mat2);
//   res->add_shape(new_object<Sphere>(Point3f(-4.f, 1.f, 0.f), 1.f, mat2));

//   Metal* mat3 = new_object<Metal>(Color(0.9f, 0.0f, 0.0f), 0.f);
//   res->materials.push_back(mat3);
//   res->add_shape(new_object<Sphere>(Point3f(4.f, 1.f, 0.f), 1.f, mat3));

//   return res;
// }


// WorldResources* setup_world_bunny() {
//   auto begin = clock();
//   WorldResources *res = new WorldResources;

//   Assimp::Importer importer;

//   const aiScene* scene = importer.ReadFile(std::string("C:\\Users\\mike\\code\\raytracing-cuda\\bunny.obj"), 0);
//   std::cout << "Assimp load time: " << (clock() - begin) * 0.001 << "s" << std::endl;

//   if (scene == nullptr) {
//     std::cerr << "err" << std::endl;
//     std::cerr << importer.GetErrorString() << std::endl;
//     delete scene;
//     return false;
//   }

//   Material ground = new_object<Lambertian>(Color(0.2f, 0.54f, 0.2f));
//   res->add_shape(new_object<Sphere>(Point3f(0.f, -1000.5f, -3.f), 1000.f, ground));
//   // res->add_shape(new_object<Sphere>(Point3f(-1.f, 1.f, -2.f), 1.f, ground));


//   Material bunny_mat = new_object<Lambertian>(Color(.91f, .99f, .99f));
//   res->materials.push_back(bunny_mat);

//   Mesh bunny("C:\\Users\\mike\\code\\raytracing-cuda\\bunny.obj", bunny_mat);
//   bunny.scale(2.f);
//   bunny.rotate_y(5);
//   bunny.translate(Vec3f(.05f, -.570f, -1.f));
//   res->append_shapes(bunny.triangles);



//   Metal* mirror = new_object<Metal>(Color(0.8f, .8f, .8f), .015f);
//   res->materials.push_back(mirror);
//   Vec3f top = Point3f(0.f, 0.3f, 0.f);
//   Vec3f right = Point3f(0.3f, -0.3f, 0.f);
//   Vec3f left  = Point3f(-0.3f, -0.3f, 0.f);

//   Vec3f mirrors_translate(0.5f, -.15f, -1.5f);

//   Triangle* t = new_object<Triangle>(top, left, right, mirror);
//   t->rotate_y(-30);
//   t->translate(mirrors_translate);
//   res->add_shape(t);

//   Metal* black = new_object<Metal>(Color(0.01f, .01f, .01f), .9f);
//   t = new_object<Triangle>(top, left, right, black);
//   t->rotate_y(-30);
//   t->scale(1.05f);
//   t->translate(mirrors_translate);
//   t->translate(Vec3f(0.f, 0.f, -.01f));
//   res->add_shape(t);

//   return res;
// }

WorldResources* setup_world() {
  WorldResources *res = new WorldResources;

  // Material grey_mat = new_object<Lambertian>(Color(0.7f, 0.7f, 0.7f));
  Material grey_mat = res->new_material<Lambertian>(Color(0.7f, 0.7f, 0.7f));
  Mesh ground = Mesh::create_plane(grey_mat);
  ground.scale(100.f);
  ground.rotate_x(-90);
  ground.translate(Vec3f(0.f, -.5f, -1.f));
  res->add_mesh(ground);

  Material left_wall_mat = res->new_material<Lambertian>(Color(.65f, .05f, .05f));
  Mesh left = Mesh::create_plane(left_wall_mat);
  left.rotate_y(90);
  left.translate(Vec3f(-.46f, -.5f, 0.f));
  res->add_mesh(left);

  Material right_wall_mat = res->new_material<Lambertian>(Color(.12f, 0.45f, 0.15f));
  Mesh right = Mesh::create_plane(right_wall_mat);
  right.rotate_y(-90);
  right.translate(Vec3f(.46f, -.5f, 0.f));
  res->add_mesh(right);

  Mesh back = Mesh::create_plane(grey_mat);
  back.translate(Vec3f(0.f, 0.f, -1.f));
  res->add_mesh(back);

  Mesh ceil = Mesh::create_plane(grey_mat);
  ceil.scale(100.f);
  ceil.rotate_x(90);
  ceil.translate(Vec3f(0.f, .5f, -1.f));
  res->add_mesh(ceil);

  Material light_mat = res->new_material<DiffuseLight>(Color(15.f, 15.f, 15.f));
  Mesh light = Mesh::create_plane(light_mat);
  light.scale(0.125f);
  light.rotate_x(90);
  light.translate(Vec3f(0.f, .499f, -.5f));
  res->add_mesh(light);

  auto scale = 0.16f;

  Mesh box("C:\\Users\\mike\\code\\raytracing-cuda\\box.fbx", grey_mat);
  // box.stretch(Vec3f(2.f, 1.f, 1.f));
  box.scale(0.15f);
  box.rotate_y(-15);
  box.translate(Vec3f(.15f, -.36f, -.35f));
  res->add_mesh(box);


  Mesh left_box("C:\\Users\\mike\\code\\raytracing-cuda\\box.fbx", grey_mat);
  // left_box.stretch(Vec3f(2.f, 1.f, 1.f));
  left_box.scale(0.15f);
  left_box.stretch(Vec3f(1.f, 2.1f, 1.f));
  left_box.rotate_y(15);
  left_box.translate(Vec3f(-.18f, -.2f, -.65f));
  res->add_mesh(left_box);

  return res;
}

int main(void) {
  auto begin = clock();

  int image_width = 800;
  float aspect_ratio = 1.f;
  int num_samples = 100;
  int max_depth = 50;
  Point3f lookfrom(0.f, 0.f, 2.f);
  Point3f lookat(0.f, 0.f, -1.f);
  Point3f vup(0.f, 1.f, 0.f);
  auto vfov = 27.f;
  auto aperture = .0001f;
  auto dist_to_focus = 1000.f;
  Camera camera(
    lookfrom,
    lookat,
    vup,
    vfov,
    aspect_ratio,
    aperture,
    dist_to_focus
  );
  std::cout.precision(2);

  World *world = new_object<World>();
  auto before_world = clock();
  WorldResources* res = setup_world();
  std::cout << "World load time: " << (clock() - begin) * 0.001 << "s" << std::endl;
  auto before_bvh = clock();
  world->construct_bvh(res->get_shapes());
  std::cout << "BVH construction time: " << (clock() - begin) * 0.001 << "s" << std::endl;

  ImageConfig* cfg = new_object<ImageConfig>(image_width, aspect_ratio, num_samples, max_depth, camera, world);

  #ifdef USE_GPU
    std::cerr << "GPU Mode" << std::endl;
    std::cout << "Total world setup time: " << (clock() - begin) * 0.001 << "s" << std::endl;

    auto render_begin = clock();
    raytrace_gpu(cfg);
    std::cout << "Render time: " << (clock() - render_begin) * 0.001 << "s" << std::endl;

  #else

    std::cerr << "CPU Mode" << std::endl;
    auto render_begin = clock();
    raytrace_cpu(*cfg);
    std::cout << "Render time: " << (clock() - render_begin) * 0.001 << "s" << std::endl;
    std::cout << std::endl;

  #endif
  std::cout << "Runtime: " << (clock() - begin) * 0.001 << "s" << std::endl;
  res->free();

  #ifdef USE_GPU
  cudaDeviceReset();
  #endif

  return 0;
}
