#include <raytrace/shape.h>
#include <util.h>
#include <math.h>
#include <rand.h>

#include <algorithm>

bool Sphere::intersect(const Ray& ray, float t_min, float t_max, Intersection &isect) const {
    auto oc = ray.origin - center;
    auto a = dot(ray.direction, ray.direction);
    auto b = 2 * dot(oc, ray.direction);
    auto c = dot(oc, oc) - (radius * radius);

    auto discriminant = (b * b) - (4 * a * c);
    auto sqrtd = sqrtf(discriminant);

    if (discriminant < 0) return false;

    auto t_hit = (-b - sqrtd) / (2.f * a);
    if (t_hit < t_min || t_hit > t_max) {
        t_hit = (-b + sqrtd) / (2.f * a);
        if (t_hit < t_min || t_hit > t_max) {
            return false;
        }
    }

    auto hit_point = ray.at(t_hit);
    auto outward_normal = (hit_point - center) / radius;
    auto front_face = dot(ray.direction, outward_normal) < 0;
    auto normal = front_face ? outward_normal : -1.f * outward_normal;

    isect.p = hit_point;
    isect.t = t_hit;
    isect.normal = normal;
    isect.front_face = front_face;
    // isect.shape = this;
    // isect.shape_type = sphere_type;
    isect.material = material;
    return true;
}

bool Sphere::bounding_box(AABB& output) const {
    output = AABB(
        center - Vec3f(radius, radius, radius),
        center + Vec3f(radius, radius, radius)
    );
    return true;
}

// void Sphere::print() const {
//     printf("Sphere(center=(%f, %f, %f), radius=%f)\n", center.x, center.y, center.z, radius);
// }

bool Triangle::intersect(const Ray& ray, float t_min, float t_max, Intersection &isect) const {
    // get the plane the triangle is in
    auto v0v1 = v1 - v0;
    auto v0v2 = v2 - v0;
    auto normal = cross(v0v1, v0v2);

    // "D" in Ax + Bx + Cx + D = 0
    auto d = -1.f * dot(normal, v0);

    // if denom is very small, then we missed the plane
    auto denom = dot(normal, ray.direction);
    if (fabsf(denom) < 1e-6f) return false;

    // calculate t and make sure it's in the range we're looking at
    auto t = -1.f * (dot(normal, ray.origin) + d) / denom;
    if (t < t_min || t > t_max) return false;

    // determine hit point on the plane using t
    auto p = ray.at(t);

    // use geometry to test whether the point on the plane is inside the triangle
    auto edge0 = v1 - v0;
    auto edge1 = v2 - v1;
    auto edge2 = v0 - v2;

    auto c0 = p - v0;
    auto c1 = p - v1;
    auto c2 = p - v2;

    auto test0 = dot(normal, cross(edge0, c0)) > 0;
    auto test1 = dot(normal, cross(edge1, c1)) > 0;
    auto test2 = dot(normal, cross(edge2, c2)) > 0;

    if (test0 && test1 && test2) {
        isect.p = p;
        isect.normal = normalize(normal);
        isect.t = t;
        isect.front_face = true; // TODO
        // isect.shape = this;
        // isect.shape_type = triangle_type;
        isect.material = this->material;
        return true;
    }

    return false;
}

bool Triangle::bounding_box(AABB& output) const {
    float min_x = fminf(v0.x, fminf(v1.x, v2.x));
    float min_y = fminf(v0.y, fminf(v1.y, v2.y));
    float min_z = fminf(v0.z, fminf(v1.z, v2.z));
    float max_x = fmaxf(v0.x, fmaxf(v1.x, v2.x));
    float max_y = fmaxf(v0.y, fmaxf(v1.y, v2.y));
    float max_z = fmaxf(v0.z, fmaxf(v1.z, v2.z));

    auto small = Point3f(min_x, min_y, min_z);
    auto big = Point3f(max_x, max_y, max_z);

    // Make sure all triangles' bounding boxes have nonzero area.
    // If a BVH node's AABB has zero area, nothing will ever intersect it.
    // There is very likely a better way to do this.
    for (int a = 0; a < 3; a++) {
        if (small[a] == big[a]) {
            big[a] += 0.0001f;
        }
    }

    output = AABB(small, big);
    return true;
}

// void Triangle::print() const {
//     printf("Triangle(v0=(%f, %f, %f), v1=(%f, %f, %f), v2=(%f, %f, %f))\n", v0.x, v0.y, v0.z, v1.x, v1.y, v1.z, v2.x, v2.y, v2.z);
// }

Vec3f Triangle::get_barycentric(const Point3f &p) const {
    auto e0 = v1 - v0;
    auto e1 = v2 - v0;
    auto e2 = p - v0;

    auto d00 = dot(e0, e0);
    auto d01 = dot(e0, e1);
    auto d11 = dot(e1, e1);
    auto d20 = dot(e2, e0);
    auto d21 = dot(e2, e1);
    auto denom = (d00 * d11) - (d01 * d01);
    auto v = (d11 * d20 - d01 * d21) / denom;
    auto w = (d00 * d21 - d01 * d20) / denom;
    auto u = 1 - v - w;

    return Vec3f(u, v, w);
}

Point3f rotate_point_x(Point3f p, float degrees) {
    auto theta = degrees_to_radians(degrees);
    auto y = cos(theta) * p.y - sin(theta) * p.z;
    auto z = sin(theta) * p.y + cos(theta) * p.z;

    return Point3f(p.x, y, z);
}
void Triangle::rotate_x(float degrees) {
    v0 = rotate_point_x(v0, degrees);
    v1 = rotate_point_x(v1, degrees);
    v2 = rotate_point_x(v2, degrees);
}

Point3f rotate_point_y(Point3f p, float degrees) {
    auto theta = degrees_to_radians(degrees);
    auto x = cos(theta) * p.x + sin(theta) * p.z;
    auto z = -sin(theta) * p.x + cos(theta) * p.z;

    return Point3f(x, p.y, z);
}

void Triangle::rotate_y(float degrees) {
    v0 = rotate_point_y(v0, degrees);
    v1 = rotate_point_y(v1, degrees);
    v2 = rotate_point_y(v2, degrees);
}

void Triangle::scale(float amount) {
    v0 = v0 * amount;
    v1 = v1 * amount;
    v2 = v2 * amount;
}

void Triangle::stretch(Vec3f magnitudes) {
    v0 = v0 * magnitudes;
    v1 = v1 * magnitudes;
    v2 = v2 * magnitudes;
}


Point3f rotate_point_z(Point3f p, float degrees) {
    auto theta = degrees_to_radians(degrees);
    auto x = cos(theta) * p.x - sin(theta) * p.y;
    auto y = sin(theta) * p.x + cos(theta) * p.y;

    return Point3f(x, y, p.z);
}

void Triangle::rotate_z(float degrees) {
    v0 = rotate_point_z(v0, degrees);
    v1 = rotate_point_z(v1, degrees);
    v2 = rotate_point_z(v2, degrees);
}

void Triangle::translate(Vec3f v) {
    v0 = v0 + v;
    v1 = v1 + v;
    v2 = v2 + v;
}

bool Shape::intersect(const Ray& ray, float t_min, float t_max, Intersection &isect) const {
  auto i = [&](auto ptr) { return ptr->intersect(ray, t_min, t_max, isect); };
  return Dispatch(i);
}

bool Shape::bounding_box(AABB& aabb) const {
  auto i = [&](auto ptr) { return ptr->bounding_box(aabb); };
  return DispatchCPU(i);
}

bool Shape::is_primitive() const {
  auto i = [&](auto ptr) { return ptr->is_primitive(); };
  return Dispatch(i);
}

void Shape::print() const {
  auto i = [&](auto ptr) { return ptr->print(); };
  return Dispatch(i);
}

void print_vec(const Vec3f& v) {
    printf("[%f, %f, %f]", v.x, v.y, v.z);
}

void Sphere::print() const {
    printf("Sphere(center=");
    print_vec(center);
    printf(", radius=%f)", radius);
}

void Triangle::print() const {
    printf("Triangle(v0=");
    print_vec(v0);
    printf(", v1=");
    print_vec(v1);
    printf(", v2=");
    print_vec(v2);
    printf(")");
}

void bvh_helper(std::string &prefix, Shape node, bool isLeft) {
    printf("%s", prefix.c_str());
    printf(isLeft ? "|--" : "L--");

    if (node.is_primitive()) {
        node.print();
        printf("\n");
    } else {
        BVHNode* bvh_node = node.Cast<BVHNode>();
        printf("AABB(minimum=");
        print_vec(bvh_node->box.minimum);
        printf(", maximum=");
        print_vec(bvh_node->box.maximum);
        printf(")\n");
        bvh_helper(prefix + (isLeft ? "|   " : "    "), bvh_node->right, true);
        bvh_helper(prefix + (isLeft ? "|   " : "    "), bvh_node->left, false);
    }
}

void BVHNode::print() const {
    bvh_helper(std::string(""), this, false);
}

CPU_GPU
bool BVHNode::intersect(const Ray& ray, float t_min, float t_max, Intersection &isect) const {
     if (!box.hit(ray, t_min, t_max))
         return false;

     return true;
}


bool BVHNode::bounding_box(AABB& aabb) const {
    aabb = box;
    return true;
}

inline bool box_compare(const Shape a, const Shape b, int axis) {
    AABB box_a;
    AABB box_b;

    if (!a.bounding_box(box_a) || !b.bounding_box(box_b))
        printf("No bounding box in bvh_node constructor. (box_compare)\n");

    return box_a.min()[axis] < box_b.min()[axis];
}

bool box_x_compare (const Shape a, const Shape b) {
    return box_compare(a, b, 0);
}

bool box_y_compare (const Shape a, const Shape b) {
    return box_compare(a, b, 1);
}

bool box_z_compare (const Shape a, const Shape b) {
    return box_compare(a, b, 2);
}

AABB surrounding_box(AABB box0, AABB box1) {
    Point3f small(fminf(box0.min().x, box1.min().x),
        fminf(box0.min().y, box1.min().y),
        fminf(box0.min().z, box1.min().z));

    Point3f big(fmaxf(box0.max().x, box1.max().x),
        fmaxf(box0.max().y, box1.max().y),
        fmaxf(box0.max().z, box1.max().z));

    return AABB(small, big);
}

BVHNode::BVHNode(
    std::vector<Shape>& src_objects,
    size_t start,
    size_t end
) {
    auto objects = src_objects; // Create a modifiable array of the source scene objects

    int axis = host_random_int(0,2);
    auto comparator = (axis == 0) ? box_x_compare
                    : (axis == 1) ? box_y_compare
                                  : box_z_compare;

    size_t object_span = end - start;

    if (object_span == 1) {
        left = right = objects[start];
    } else if (object_span == 2) {
        if (comparator(objects[start], objects[start+1])) {
            left = objects[start];
            right = objects[start+1];
        } else {
            left = objects[start+1];
            right = objects[start];
        }
    } else {
        std::sort(objects.begin() + start, objects.begin() + end, comparator);

        auto mid = start + object_span/2;
        left = new_object<BVHNode>(objects, start, mid);
        right = new_object<BVHNode>(objects, mid, end);
    }

    AABB box_left, box_right;

    if (  !left.bounding_box(box_left)
       || !right.bounding_box(box_right)
    )
        std::cerr << "No bounding box in bvh_node constructor.\n";

    box = surrounding_box(box_left, box_right);
}
