#ifndef RAYTRACE_H
#define RAYTRACE_H

#include <defines.h>
#include <img.h>
#include <raytrace/camera.h>

#ifdef USE_GPU
__device__
#else
__host__ __device__
#endif
Color raytrace(ImageConfig &cfg, int i, int j);

#endif
