#ifndef INTERSECTION_H
#define INTERSECTION_H

#include <vec.h>
#include <raytrace/material.h>

class Intersection {
public:
    Point3f p;
    Vec3f normal;
    float t;
    bool front_face;

    Material material;

    CPU_GPU
    Intersection() : p(Point3f(0.f, 0.f, 0.f)), normal(Vec3f(0.f, 0.f, 0.f)) {}
    // Intersection() {};
};

#endif
