#ifndef WORLD_H
#define WORLD_H

#include <algorithm>

#include <defines.h>
#include <gpu.h>
#include <raytrace/shape.h>
#include <raytrace/ray.h>
#include <util.h>

#include <cuda_runtime_api.h>
#include <cuda.h>


class World {
public:
    BVHNode bvhRoot;

    World() : bvhRoot() {}

    void construct_bvh(std::vector<Shape>& shapes) {
        bvhRoot = BVHNode(shapes);
        // bvhRoot.print();
    }

    CPU_GPU bool closest_intersect(Ray& ray, float t_min, float t_max, Intersection& isect);
};

#endif
