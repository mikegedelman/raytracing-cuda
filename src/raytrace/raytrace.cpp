#include <cmath>

#include <defines.h>
#include <vec.h>
#include <img.h>
#include <raytrace/camera.h>
#include <raytrace/raytrace.h>
#include <raytrace/shape.h>
#include <raytrace/material.h>
#include <util.h>


CPU_GPU
Color color_ray(ImageConfig &cfg, Ray &ray, int depth) {
  // Color ret(0.f);

  Color accum_attenuation(1.f);
  Color light(0.f);

  Ray cur_ray = ray;
  for (int i = depth; i >= 0; i--) {
    if (depth <= 0) {
      return Color(0.f);
    }


    Intersection isect;
    if (cfg.world->closest_intersect(cur_ray, 0.000001f, INFINITY, isect)) {//

      Ray scattered;
      Color attenuation;
      Color emitted = isect.material.emitted();

      if (isect.material.scatter(cur_ray, &isect, scattered, attenuation)) {
        cur_ray = scattered;
        cur_ray.origin = cur_ray.at(0.001f);
        // ret = ret * attenuation;
        accum_attenuation = accum_attenuation * attenuation;
        light = light + emitted;
        continue;
      } else {
        light = emitted;
        break;
      }
    }

    break;
  }

  return light * accum_attenuation;
  // Color blue = Color(.5f, .7f, 1.f);
  // Color white = Color(1.f, 1.f, 1.f);

  // float t = 0.5f * (normalize(cur_ray.direction).y + 1.f);
  // return ret * (((1.0f - t) * white) + (t * blue));
}

CPU_GPU
Color gamma_correct(Color color) {
  return Color(sqrtf(color.x), sqrtf(color.y), sqrtf(color.z));
  // return color;
}


Color raytrace(ImageConfig &cfg, int i, int j) {
  Color pixel_color = Color(0.f, 0.f, 0.f);

  for (int s = 0; s < cfg.samples; s++) {
    auto u = float(i + random_float()) / (cfg.width - 1);
    auto v = float(j + random_float()) / (cfg.height - 1);

    Ray ray = cfg.camera.get_ray(u, v);
    Color c = color_ray(cfg, ray, 50);
    pixel_color = pixel_color + c;
  }
  pixel_color = pixel_color / float(cfg.samples);
  pixel_color = gamma_correct(pixel_color);
  return pixel_color;
}
