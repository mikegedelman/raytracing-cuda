#ifndef AABB_H
#define AABB_H

#include <vec.h>
#include <raytrace/ray.h>

class AABB {
    public:
        CPU_GPU2 AABB() {}
        CPU_GPU2 AABB(const Point3f& a, const Point3f& b) { minimum = a; maximum = b;}

        CPU_GPU2 Point3f min() const {return minimum; }
        CPU_GPU2 Point3f max() const {return maximum; }

        CPU_GPU2
        inline bool AABB::hit(const Ray& r, float t_min, float t_max) const {
            for (int a = 0; a < 3; a++) {
                auto invD = 1.0f / r.direction[a];
                auto t0 = (min()[a] - r.origin[a]) * invD;
                auto t1 = (max()[a] - r.origin[a]) * invD;
                if (invD < 0.0f) {
                    // std::swap not available on the gpu
                    float tmp = t0;
                    t0 = t1;
                    t1 = tmp;
                }
                t_min = t0 > t_min ? t0 : t_min;
                t_max = t1 < t_max ? t1 : t_max;
                if (t_max <= t_min)
                    return false;
            }
            return true;
        }

        Point3f minimum;
        Point3f maximum;

        void print() const;
};

AABB surrounding_box(AABB box0, AABB box1);

#endif
