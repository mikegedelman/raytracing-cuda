#ifndef RAY_H
#define RAY_H

#include <defines.h>
#include <vec.h>

class Ray {
public:
    Point3f origin;
    Vec3f direction;

    CPU_GPU
    Ray() {}

    CPU_GPU
    Ray(Point3f origin, Vec3f direction) : origin(origin), direction(direction) {};

    CPU_GPU
    Point3f at(const float t) const {
        return origin + (direction * t);
    }
};

#endif
