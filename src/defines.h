#ifndef DEFINES_H
#define DEFINES_H

// USE_GPU is defined in CMakeLists.txt

#ifdef USE_GPU
#define CPU_GPU __device__
#define CPU_GPU2 __host__ __device__
#else
#define CPU_GPU
#define CPU_GPU2
#endif

// These defines to make linter/ide happy
#ifndef __host__
#define __host__
#endif

#ifndef __device__
#define __device__
#endif

#ifndef __global__
#define __global__
#endif

#endif
