#ifndef UTIL_H
#define UTIL_H

#include <rand.h>
#include <vec.h>
#include <gpu.h>

constexpr float pi = 3.1415926535897932385f;
constexpr float close_to_zero = 1e-8f;

inline float clamp(float x, float min, float max) {
    if (x < min) return min;
    if (x > max) return max;
    return x;
}

__host__ __device__
inline float degrees_to_radians(float degrees) {
    return degrees * pi / 180.0f;
}

CPU_GPU
inline bool near_zero(Vec3f &v) {
    auto s = close_to_zero;
    return (fabsf(v.x) < s) && (fabsf(v.y) < s) && (fabsf(v.x) < s);
}


template <class T, class... Args>
inline T *new_object(Args &&...args) {
    #ifdef USE_GPU
    T *p;
    CUDA_CHECK( cudaMallocManaged(&p, sizeof(T)) );
    return ::new ((void *)p) T(std::forward<Args>(args)...);
    #else
    return new T(std::forward<Args>(args)...);
    #endif
}
template <class T>
inline void delete_object(T *p) {
    #ifdef USE_GPU
    p->~T();
    CUDA_CHECK( cudaFree(p) );
    #else
    delete p;
    #endif
}

#endif
