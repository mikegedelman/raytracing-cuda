#include <raytrace/material.h>
#include <raytrace/intersection.h>
#include <util.h>

CPU_GPU
Vec3f reflect(const Vec3f& v, const Vec3f& n) {
    return v - 2 * dot(v, n) * n;
}

CPU_GPU
bool Lambertian::scatter(Ray _r, Intersection *isect, Ray &scattered, Color &attenuation) const {
    Point3f target = isect->p + isect->normal + random_unit_vec3f();
    Vec3f scatter_direction = target - isect->p;

    // Catch degenerate scatter direction
    if (near_zero(scatter_direction)) {
        scatter_direction = isect->normal;
    }

    scattered = Ray(isect->p, scatter_direction);
    attenuation = albedo;
    return true;
}

CPU_GPU
bool Metal::scatter(Ray ray, Intersection *isect, Ray &scattered, Color &attenuation) const {
    Vec3f reflected = reflect(normalize(ray.direction), isect->normal);
    scattered = Ray(isect->p, reflected + fuzz * random_vec3f_in_unit_sphere() );
    attenuation = albedo;
    return dot(scattered.direction, isect->normal) > 0;
}

CPU_GPU
Vec3f refract(const Vec3f& uv, const Vec3f& n, float etai_over_etat) {
    auto cos_theta = fminf(dot(-1.f * uv, n), 1.0f);
    Vec3f r_out_perp =  etai_over_etat * (uv + cos_theta*n);
    Vec3f r_out_parallel = -sqrtf(fabs(1.0 - length_squared(r_out_perp))) * n;
    return r_out_perp + r_out_parallel;
}

CPU_GPU
float reflectance(float cosine, float ref_idx) {
    auto r0 = (1-ref_idx) / (1+ref_idx);
    r0 = r0*r0;
    return r0 + (1-r0)*pow((1 - cosine),5);
}

CPU_GPU
bool Dielectric::scatter(Ray ray, Intersection *isect, Ray &scattered, Color &attenuation) const {
    attenuation = Color(1.f);
    float refraction_ratio = isect->front_face ? (1.0f / ir) : ir;

    Vec3f unit_direction = normalize(ray.direction);
    float cos_theta = fminf(dot(-1.f * unit_direction, isect->normal), 1.f);
    float sin_theta = sqrtf(1.f - cos_theta * cos_theta);

    bool cannot_refract = refraction_ratio * sin_theta > 1.f;
    Vec3f direction;
    if (cannot_refract || reflectance(cos_theta, refraction_ratio) > random_float()) {
        direction = reflect(unit_direction, isect->normal);
    } else {
        direction = refract(unit_direction, isect->normal, refraction_ratio);
    }

    scattered = Ray(isect->p, direction);
    return true;
}

CPU_GPU
bool Material::scatter(Ray ray, Intersection *isect, Ray &scattered, Color &attenuation) const {
    auto i = [&](auto ptr) { return ptr->scatter(ray, isect, scattered, attenuation); };
  return Dispatch(i);
}

CPU_GPU
Color Material::emitted() const {
  auto i = [&](auto ptr) { return ptr->emitted(); };
  return Dispatch(i);
}
