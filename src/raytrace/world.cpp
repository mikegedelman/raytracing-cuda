#include <raytrace/world.h>

template<class T>
class Stack {
    T buf[64];
    size_t idx;
public:
    CPU_GPU
    Stack() : buf(), idx(0) {}

    CPU_GPU void push(T val) {
        buf[idx++] = val;
    }
    CPU_GPU T pop() {
        idx--;
        return buf[idx];
    }
    CPU_GPU bool empty() { return idx <= 0; }

};

bool World::closest_intersect(Ray& ray, float t_min, float t_max, Intersection& isect) {
    Stack<Shape> stack;
    stack.push(&bvhRoot);

    float t_max_2 = t_max;
    bool hit_any = false;

    while (!stack.empty()) {
        Shape cur = stack.pop();
        if (cur.intersect(ray, t_min, t_max_2, isect)) {

            if (cur.is_primitive()) {
                t_max_2 = isect.t;
                hit_any = true;
            }
            else {
                BVHNode* node = cur.Cast<BVHNode>();
                stack.push(node->left);
                stack.push(node->right);
            }
        }
    }

    return hit_any;
}
