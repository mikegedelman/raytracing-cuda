#ifndef VEC_H
#define VEC_H

#include <iostream>
#include <cmath>
#include <assert.h>

#include <defines.h>

template <class T>
class Vec3 {
public:
    T x;
    T y;
    T z;

    CPU_GPU2
    Vec3() {}

    CPU_GPU2
    Vec3(T x, T y, T z) : x(x), y(y), z(z) {}

    CPU_GPU2
    Vec3(T a) : x(a), y(a), z(a) {}

    // CPU_GPU2 Vec3<T> operator-() const;
    // CPU_GPU2 Vec3<T>& operator+= (const Vec3<T>&);
    // CPU_GPU2 Vec3<T>& operator-= (const Vec3<T>&);
    // CPU_GPU2 Vec3<T>& operator*= (const Vec3<T>&);
    // CPU_GPU2 Vec3<T>& operator/= (const Vec3<T>&);

    CPU_GPU2
    T operator[](int i) const {
        if (i == 0) return x;
        if (i == 1) return y;
        if (i == 2) return z;
        assert(i <= 3);
    }
    CPU_GPU2
    T& operator[](int i) {
        if (i == 0) return x;
        if (i == 1) return y;
        if (i == 2) return z;
        assert(i <= 3);
    }
};

typedef Vec3<float> Vec3f;
typedef Vec3<int> Vec3i;
typedef Vec3f Color;
typedef Vec3f Point3f;


template <class T>
CPU_GPU2
inline std::ostream& operator<<(std::ostream &out, const Vec3<T> &v) {
    return out << "Vec3(" << v.x << ", " << v.y << ", " << v.z << ")";
}

template <class T>
CPU_GPU2
inline Vec3<T> operator+(const Vec3<T> &u, const Vec3<T> &v) {
    return Vec3<T>(u.x + v.x, u.y + v.y, u.z + v.z);
}

template <class T>
CPU_GPU2
inline Vec3<T> operator+(const Vec3<T> &u, const T f) {
    return Vec3<T>(u.x + f, u.y + f, u.z + f);
}

template <class T>
CPU_GPU2
inline Vec3<T> operator+(const T f, const Vec3<T> &u) {
    return u + f;
}

template <class T>
CPU_GPU2
inline Vec3<T> operator-(const Vec3<T> &u, const Vec3<T> &v) {
    return Vec3<T>(u.x - v.x, u.y - v.y, u.z - v.z);
}

template <class T>
CPU_GPU2
inline Vec3<T> operator-(const Vec3<T> &u, const T f) {
    return Vec3<T>(u.x - f, u.y - f, u.z - f);
}

template <class T>
CPU_GPU2
inline Vec3<T> operator-(const T f, const Vec3<T> &u) {
    return u - f;
}

template <class T>
CPU_GPU2
inline Vec3<T> operator*(const Vec3<T> &u, const Vec3<T> &v) {
    return Vec3<T>(u.x * v.x, u.y * v.y, u.z * v.z);
}

template <class T>
CPU_GPU2
inline Vec3<T> operator*(const Vec3<T> &u, const T f) {
    return Vec3<T>(u.x * f, u.y * f, u.z * f);
}

template <class T>
CPU_GPU2
inline Vec3<T> operator*(const T f, const Vec3<T> &u) {
    return u * f;
}

template <class T>
CPU_GPU2
inline Vec3<T> operator/(const Vec3<T> &u, const Vec3<T> &v) {
    return Vec3<T>(u.x / v.x, u.y / v.y, u.z / v.z);
}

template <class T>
CPU_GPU2
inline Vec3<T> operator/(const Vec3<T> &u, const T f) {
    return Vec3<T>(u.x / f, u.y / f, u.z / f);
}

template <class T>
CPU_GPU2
inline T dot(const Vec3<T> &u, const Vec3<T> &v) {
    return u.x * v.x + u.y * v.y + u.z * v.z;
}

template <class T>
CPU_GPU2
inline Vec3<T> cross(const Vec3<T> &u, const Vec3<T> &v) {
    return Vec3<T>(
        (u.y * v.z) - (u.z * v.y),
        (u.z * v.x) - (u.x * v.z),
        (u.x * v.y) - (u.y * v.x)
    );
}


template <class T>
CPU_GPU2
inline T length_squared(const Vec3<T> &v) {
    return v.x * v.x + v.y * v.y + v.z * v.z;
}

template <class T>
CPU_GPU2
inline T length(const Vec3<T> &v) {
    return sqrtf(length_squared(v));
}

template <class T>
CPU_GPU2
inline Vec3<T> normalize(const Vec3<T> &v) {
    return v / length(v);
}

#endif
