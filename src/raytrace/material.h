#ifndef MATERIAL_H
#define MATERIAL_H

#include <vec.h>
#include <rand.h>
#include <taggedptr.h>
#include <raytrace/ray.h>

class Intersection;

enum MaterialType {
    lambertian_type
};

class Lambertian;
class Metal;
class Dielectric;
class DiffuseLight;

class Material : public TaggedPointer<Lambertian, Metal, Dielectric, DiffuseLight> {
public:
    using TaggedPointer::TaggedPointer;

    CPU_GPU
    bool scatter(Ray ray, Intersection *isect, Ray &scattered, Color &attenuation) const;

    CPU_GPU
    Color emitted() const;
};

class Lambertian : public Material {
public:
    Color albedo;

    Lambertian(Color albedo) : albedo(albedo) {}

    CPU_GPU
    bool scatter(Ray ray, Intersection *isect, Ray &scattered, Color &attenuation) const;

    CPU_GPU
    Color emitted() const { return Color(0.f, 0.f, 0.f); }
};

class Metal : public Material {
public:
    Color albedo;
    float fuzz;

    Metal(Color albedo, float fuzz) : albedo(albedo), fuzz(fuzz) {}

    CPU_GPU
    bool scatter(Ray ray, Intersection *isect, Ray &scattered, Color &attenuation) const;

    CPU_GPU
    Color emitted() const { return Color(0.f, 0.f, 0.f); }
};

class Dielectric : public Material {
public:
    float ir; // index of refraction

    Dielectric(float index_of_refraction) : ir(index_of_refraction) {}

    CPU_GPU
    bool scatter(Ray ray, Intersection *isect, Ray &scattered, Color &attenuation) const;

    CPU_GPU
    Color emitted() const { return Color(0.f, 0.f, 0.f); }
};

class DiffuseLight : public Material {
    Color light_color;

public:
    DiffuseLight(Color light_color) : light_color(light_color) {};

    CPU_GPU
    bool scatter(Ray ray, Intersection *isect, Ray &scattered, Color &attenuation) const { return false; }

    CPU_GPU
    Color emitted() const {
        return light_color;
    }
};

#endif
