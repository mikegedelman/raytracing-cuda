#ifndef RAND_H
#define RAND_H

#include <cstdlib>
#include <defines.h>
#include <vec.h>

#include <curand.h>
#include <curand_kernel.h>

__device__ extern curandState* curand_state;
__device__ extern int img_width;


inline float host_random_float() {
    return (float) rand() / (RAND_MAX + 1.0f);
}

inline float host_random_float(float min, float max) {
    // Returns a random real in [min,max).
    return min + (max-min) * (float) host_random_float();
}

inline int host_random_int(int min, int max) {
    // Returns a random int in [min,max].
    return static_cast<int>(host_random_float(min, max+1));
}

inline Vec3f host_random_vec3f() {
    return Vec3f(host_random_float(), host_random_float(), host_random_float());
}

inline Vec3f host_random_vec3f(float min, float max) {
    return Vec3f(host_random_float(min, max), host_random_float(min, max), host_random_float(min, max));
}


CPU_GPU
inline float random_float() {
    #ifdef USE_GPU
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    int id = (j * img_width) + i;
    return curand_uniform(&curand_state[id]);
    #else
    return host_random_float();
    #endif
}

CPU_GPU
inline float random_float(float min, float max) {
    // Returns a random real in [min,max).
    return min + (max-min) * (float) random_float();
}


CPU_GPU
inline int random_int(int min, int max) {
    // Returns a random int in [min,max].
    return static_cast<int>(random_float(min, max+1));
}


CPU_GPU
inline Vec3f random_vec3f() {
    return Vec3f(random_float(), random_float(), random_float());
}

CPU_GPU
inline Vec3f random_vec3f(float min, float max) {
    return Vec3f(random_float(min, max), random_float(min, max), random_float(min, max));
}

CPU_GPU
inline Vec3f random_vec3f_in_unit_sphere() {
    while (true) {
        auto p = random_vec3f(-1.f, 1.f);
        if (length_squared(p) >= 1) continue;
        return p;
    }
}

CPU_GPU
inline Vec3f random_unit_vec3f() {
    return normalize(random_vec3f_in_unit_sphere());
}

CPU_GPU
inline Vec3f random_in_unit_disk() {
    while (true) {
        auto p = Vec3f(random_float(-1.f,1.f), random_float(-1.f,1.f), 0.f);
        if (length_squared(p) >= 1) continue;
        return p;
    }
}

#endif
