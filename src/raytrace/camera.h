#ifndef CAMERA_H
#define CAMERA_H

#include <defines.h>
#include <vec.h>
#include <util.h>
#include <raytrace/ray.h>

class Camera {
    Point3f origin;
    Point3f lower_left_corner;
    Vec3f horizontal;
    Vec3f vertical;
    Vec3f w, u, v;
    float lens_radius;
public:
    __host__
    Camera(
        Point3f lookfrom,
        Point3f lookat,
        Vec3f vup,
        float vfov,
        float aspect_ratio,
        float aperture,
        float focus_dist
    ) {
        auto theta = degrees_to_radians(vfov);
        auto h = tanf(theta/2);
        auto viewport_height = 2.0f * h;
        auto viewport_width = aspect_ratio * viewport_height;

        w = normalize(lookfrom - lookat);
        u = normalize(cross(vup, w));
        v = cross(w, u);

        origin = lookfrom;
        horizontal = viewport_width * u;
        vertical = viewport_height * v;
        lower_left_corner = origin - horizontal/2.f - vertical/2.f - w;

        horizontal = focus_dist * viewport_width * u;
        vertical = focus_dist * viewport_height * v;
        lower_left_corner = origin - horizontal/2.f - vertical/2.f - focus_dist*w;

        lens_radius = aperture / 2;
    };

    CPU_GPU
    Ray get_ray(float s, float t) const {
        Vec3f rd = lens_radius * random_in_unit_disk();
        Vec3f offset = u * rd.x + v * rd.y;

        return Ray(
            origin + offset,
            lower_left_corner + s*horizontal + t*vertical - origin - offset
        );
    }
};

#endif
